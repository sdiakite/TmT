Compilation (Jave 8 et plus):
 - make

Installation (en tant que **root**):
 - make install

Execution:
 - java -jar build/barcode.jar

Informations:
 - L'appli selectionne par défaut la première imprimante qui contient "*tiquette*" peut importe la casse
 - Configuration de l'imprimante :
   - **Pilote** : Zebra ZPL Label Printer
   - **Nom** : Imprimante_Etiquettes
   - **Orientation** : Portrait
   - **Taille du papier** : 1.5x1 (pouces, pas la vrai dimmension, mais ça fonctionne)
   - **Résolution** : 203 PPP
   - **Crénage du papier** : Non continu (détection Web)

