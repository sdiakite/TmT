package fr.tasmeilleurtemps.barcode;
import javax.swing.UIManager;
public class Main {
    public static void main(String args[]) {
        lancerFenetre();
    }
    static void lancerFenetre() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                /*try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }*/
                com.formdev.flatlaf.FlatLightLaf.install();
                javax.swing.UIDefaults defaults = UIManager.getLookAndFeelDefaults();
                if (defaults.get("Table.alternateRowColor") == null) {
                    defaults.putIfAbsent("Table.alternateRowColor", java.awt.Color.LIGHT_GRAY);  
                }
                GUI gui = new GUI();
                gui.pack();
                gui.setVisible(true);
            }
        });
    }
}