package fr.tasmeilleurtemps.barcode;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Vector;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableColumnModel;
import java.awt.image.BufferedImage;
import java.awt.event.*;

class GUI extends JFrame implements ActionListener {

    JLabel labelFichierCSV = new JLabel(" >");
    JLabel imageLabel = new JLabel();
    JTextField checherTextField = new JTextField("");
    JTextArea nomImprimeTextField = new JTextArea("");
    ExtractionODOO odoo = new ExtractionODOO();
    BufferedImage codeBarre = null;
    BufferedImage imageBlanche = new BufferedImage(BarCode.L_PX, BarCode.H_PX, BufferedImage.TYPE_INT_RGB);
    ImageIcon visualizationCodeBarre = new ImageIcon(imageBlanche);
    SpinnerNumberModel nombreDEtiquettes = new SpinnerNumberModel(1, 1, 100, 1);
    JButton impressionButton = new JButton("--");
    JComboBox<Imprimante> imprimanteList;

    JTable table = new JTable(odoo);
    JTable jobTable = new JTable(odoo);
    

    private Properties conf = new Properties();
    private File fichierDeConf = null;

    GUI() {
        super("Imprime tes codes barres !");
        setMinimumSize(new Dimension(800, 700));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));

        setIconImage((new ImageIcon(getClass().getResource("/ressources/barcode_64.png"))).getImage());
        
        chargerImprimantes();
        impressionButton.setActionCommand("produit:print");
        impressionButton.addActionListener(this);

        JPanel mainPanel = new JPanel(new BorderLayout());

        add(mainPanel, BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                selectionProduit(table.getSelectedRow());
            }
        });
        scrollPane.setBorder(new TitledBorder("Extraction ODOO"));
        mainPanel.add(scrollPane, BorderLayout.CENTER);

        JPanel topPanel = new JPanel(new BorderLayout());
        mainPanel.add(topPanel, BorderLayout.NORTH);

        JPanel chargerPanel = new JPanel(new BorderLayout());
        chargerPanel.setBorder(new TitledBorder("Charger un fichier d'extraction ODOO .csv"));
        JButton chargerButton = new JButton("Ouvrir un csv");
        chargerPanel.add(chargerButton, BorderLayout.WEST);
        chargerPanel.add(labelFichierCSV, BorderLayout.CENTER);
        chargerButton.setActionCommand("csv:open");
        chargerButton.addActionListener(this);

        topPanel.add(chargerPanel, BorderLayout.NORTH);
        JPanel topFiltrage = new JPanel(new BorderLayout());
        topFiltrage.setBorder(new TitledBorder("Filtrage"));
        // topFiltrage.add(new JLabel(" Filtrage : "), BorderLayout.WEST);
        topFiltrage.add(checherTextField, BorderLayout.CENTER);
        topPanel.add(topFiltrage, BorderLayout.SOUTH);
        //checherTextField.setActionCommand("odoo:filter");

        JPanel panelImpression = new JPanel(new BorderLayout());
       
        panelImpression.setBorder(new TitledBorder("Apperçu impression"));
        nomImprimeTextField.setText("");
        nomImprimeTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
            }

            public void removeUpdate(DocumentEvent e) {
                changerLabelCodeBarre(table.getSelectedRow(), checherTextField.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                changerLabelCodeBarre(table.getSelectedRow(), checherTextField.getText());
            }
        });
        JPanel tmpPanel = new JPanel(new BorderLayout());
        JPanel nomImprimePanel = new JPanel();
        nomImprimePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        nomImprimePanel.add(nomImprimeTextField);
        nomImprimeTextField.setColumns(20);
        nomImprimeTextField.setRows(4);
        tmpPanel.add(nomImprimePanel, BorderLayout.NORTH);
        panelImpression.add(tmpPanel, BorderLayout.CENTER);

        JPanel impressionPanel = new JPanel(new BorderLayout());
        JSpinner nombreEtiquette = new JSpinner(nombreDEtiquettes);
        impressionButton.setEnabled(false);
        JPanel cntPanel = new JPanel(new BorderLayout());
        cntPanel.add(new JLabel("Imprimer ")       , BorderLayout.WEST  );
        cntPanel.add(nombreEtiquette               , BorderLayout.CENTER);
        cntPanel.add(new JLabel(" étiquettes sur "), BorderLayout.EAST  );
        impressionPanel.add(cntPanel        , BorderLayout.WEST);
        impressionPanel.add(imprimanteList  , BorderLayout.CENTER);
        impressionPanel.add(impressionButton, BorderLayout.EAST);
        tmpPanel.add(impressionPanel, BorderLayout.SOUTH);
        
        for (int x = 0; x < imageBlanche.getWidth(); x++) {
            for (int y = 0; y < imageBlanche.getHeight(); ++y) {
                imageBlanche.setRGB(x, y, 0xFFFFFFFF);
            }
        }
        imageLabel.setIcon(visualizationCodeBarre);
        panelImpression.add(imageLabel, BorderLayout.EAST);
        topPanel.add(panelImpression, BorderLayout.CENTER);

        checherTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {}
            public void removeUpdate(DocumentEvent e) {
                odoo.filter(checherTextField.getText());
            }
            public void insertUpdate(DocumentEvent e) {
                odoo.filter(checherTextField.getText());
            }
        });

        Timer timer = new Timer(10000, this);
        timer.setActionCommand("timer:tick");
        timer.start();

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                chargerConfig();
            }
        });

    }
    void chargerConfig() {
        String confDir = System.getProperty("user.home");
        if (null == confDir || (confDir = confDir.trim()).length() == 0) {
            Log.log("Impossible de charger la configuration, propriété java \"user.home\" absente", confDir);
        }
        confDir += "/.config/fr.tasmeilleurtemps.barcode";
        File confFile = new File(confDir);
        if (!confFile.exists() && !confFile.mkdirs()) {
            Log.log("Erreur à la création du dossier de configuration", confDir);
            return;
        }
        confFile = new File(confFile, "BarCode.properties");
        try {
            if (!confFile.exists() && !confFile.createNewFile()) {
                Log.log("Erreur à la création du fichier de configuration", confFile.getAbsolutePath());
                return;
            }
            try (FileInputStream is = new FileInputStream(confFile)) {
                conf.load(is);
            }
            fichierDeConf = confFile;
        } catch (IOException e) {
            Log.log("Erreur à la création du fichier de configuration", e);
            return;
        }
        
        String lastCSV = conf.getProperty("dernier_fichier_csv");
        if (null != lastCSV) {
            File f = new File(lastCSV);
            if (f.exists() && f.canRead()) {
                Log.log("Réouverture du dernier fichier", lastCSV);
                loadCSV(f);
            }
        }
    }
    synchronized void enregistrerConfig() {
        if (null != fichierDeConf) {
            try (FileOutputStream os = new FileOutputStream(fichierDeConf)) {
                conf.store(os, "BarCode");
            } catch(IOException e) {
                Log.log("Erreur à l'enregistrement du fichier de configuration", e);
            }
        }
    }
    void chargerImprimantes() {
        try {
            int imprimanteEtiquetteIndex = -1;
            Vector<Imprimante> imprimantes = Imprimante.list();
            imprimanteList = new JComboBox<>(imprimantes);
            for (int i = 0; i < imprimantes.size() && imprimanteEtiquetteIndex < 0; ++i) {
                if (imprimantes.get(i).name.toLowerCase().contains("tiquette")) {
                    imprimanteEtiquetteIndex = i;
                    imprimanteList.setSelectedIndex(imprimanteEtiquetteIndex);
                }
            }
        } catch(IOException|RuntimeException e) {
            Log.log("GUI.chargerImprimantes()", e);
            imprimanteList = new JComboBox<>();
            imprimanteList.setEnabled(false);
            JOptionPane.showMessageDialog(this, "Erreur à la recherche des imprimantes : \"" + e.getMessage() + "\"", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }
    void loadCSV(File f) {
        try {
            odoo.update(f, this);
            labelFichierCSV.setText(" > " + f.getAbsolutePath());
            if (odoo.getRowCount() > 0) {
                conf.setProperty("dernier_fichier_csv", f.getAbsolutePath());
                conf.setProperty("dernier_dossier_csv", f.getParentFile().getAbsolutePath());
                enregistrerConfig();
            }
        } catch(Exception e) {
            Log.log("Erreur au chargement de ", f.getAbsolutePath(), e);
            JOptionPane.showMessageDialog(this,"Erreur au chargement de " + f.getAbsolutePath(), "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }
    void openDialog() {
        File baseFolder = null;
        String lastCSV = conf.getProperty("dernier_dossier_csv");
        if (null != lastCSV) {
            baseFolder = new File(lastCSV);
            if (!baseFolder.exists() || !baseFolder.canRead() || !baseFolder.isDirectory()) {
                baseFolder = null;
            }
        }
        File zenity = new File("/usr/bin/zenity");
        if (zenity.exists() && zenity.canExecute()) {
            try {
                Processus p = new Processus(zenity.getAbsolutePath(), (null == baseFolder) ? new File(System.getProperty("user.home")) : baseFolder,
                    "--file-selection", "--title=\"Export ODOO en CSV\"", "--file-filter=\"Export ODOO en CSV | *.csv *.CSV\""
                );
                if (p.returnCode >= 0) {
                    if (p.returnCode == 0 && p.out.size() == 1) {
                        loadCSV(new File(p.out.get(0)));
                    }
                    return;
                }
            } catch (Exception e) {
                Log.log("Zenity error", e);
            }
        }
        JFileChooser fileChooser = (null == baseFolder) ? new JFileChooser() : new JFileChooser(baseFolder);
        fileChooser.setFileFilter(new FileNameExtensionFilter("Export ODOO en CSV", "csv", "CSV"));
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            loadCSV(fileChooser.getSelectedFile());
        }
    }
    void imprimer() {
        Imprimante imprimante = (Imprimante)imprimanteList.getSelectedItem();
        ExtractionODOO.Produit produit = odoo.getRow(table.getSelectedRow());
        int total = ((Integer)nombreDEtiquettes.getValue()).intValue();
        if (null != imprimante) {
            try {
                String jobName = imprimante.imprime(codeBarre, total);
                JOptionPane.showMessageDialog(this, "Impression de " + total + " " + produit.getNom() + " sur " + imprimante + "\nIdentifiant de job : \""+jobName+"\"", "Impression en cours : \""+jobName+"\"", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                Log.log("GUI.imprimer()", e);
                JOptionPane.showMessageDialog(this, "Erreur d'impression : " + e.getMessage(), "Erreur", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Pas d'imprimante selectionnée", "Erreur", JOptionPane.WARNING_MESSAGE);
        }
    }
    void majCodeBarre(ExtractionODOO.Produit produit) {
        String label = nomImprimeTextField.getText();
        BufferedImage img = BarCode.make(produit.getCodeBarre(), label.split("\n\r|\r|\n"), codeBarre);
        if (null == img) {
            img = imageBlanche;
        } else if (BarCode.MAUVAIS_CODE_BARRE != img) {
            codeBarre = img;
        }
        visualizationCodeBarre.setImage(img);
    }
    void changerLabelCodeBarre(int index, String text) {
        visualizationCodeBarre.getImage().flush();
        if (index >= 0) {
            ExtractionODOO.Produit produit = odoo.getRow(index);
            if (null == text || text.length() == 0) {
                text = produit.getNomAffiche();
            }
            majCodeBarre(produit);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    getContentPane().repaint();
                }
            });
        }
    }
    void selectionProduit(int index) {
        nombreDEtiquettes.setValue(1);
        visualizationCodeBarre.getImage().flush();
        if (index >= 0) {
            ExtractionODOO.Produit produit = odoo.getRow(index);
            nomImprimeTextField.setText(produit.getNomAffiche());
            majCodeBarre(produit);
            impressionButton.setEnabled(visualizationCodeBarre.getImage() != BarCode.MAUVAIS_CODE_BARRE);
        } else {
            nomImprimeTextField.setText("");
            visualizationCodeBarre.setImage(imageBlanche);
            impressionButton.setEnabled(false);
        }
        impressionButton.setText(impressionButton.isEnabled() ? "Go" : "--");
        //imageLabel.setIcon(visualizationCodeBarre);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                getContentPane().repaint();
            }
        });
    }
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "csv:open":
               openDialog();
               break;
            case "produit:print":
                imprimer();
                break;
            case "timer:tick":
                for (int i = 0; i < imprimanteList.getItemCount(); ++i) {
                    imprimanteList.getItemAt(i).cleanDoneJobs();
                }
                break;
            default:
                JOptionPane.showMessageDialog(this, "Action inconnue : " + e.getActionCommand(), "Action inconnue : " + e.getActionCommand(), JOptionPane.WARNING_MESSAGE);
        }
        
    }
}
