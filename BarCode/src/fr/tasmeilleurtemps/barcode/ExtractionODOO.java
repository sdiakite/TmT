package fr.tasmeilleurtemps.barcode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import java.awt.Component;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;



public class ExtractionODOO extends AbstractTableModel {

    public static class Produit {
        public static final String COL_NAMES[] = new String[] {
            "Ref interne", "Nom", "Nom court", "Code Barre"
        };
        public static final Class COL_CLASSES[] = new Class[] {
            String.class, String.class, String.class, String.class
        };
        private final String cols[];
        private final String colsMinuscules[];
        public Produit(String refInterne, String nom, String nomCourt, String codeBarre) {
            cols = new String[] { refInterne, nom, nomCourt, codeBarre };
            colsMinuscules = new String[] {
                cols[0].toLowerCase(), cols[1].toLowerCase(), cols[2].toLowerCase(), cols[3].toLowerCase()
            };
        }
        public String getColumn(int index) {
            return cols[index];
        }
        public boolean match(String filtre) {
            for (String col : colsMinuscules) {
                if (col.indexOf(filtre) >= 0) {
                    return true;
                }
            }
            return false;
        }
        public String getRef() {
            return cols[0];
        }
        public String getNom() {
            return cols[1];
        }
        public String getNomCourt() {
            return cols[2];
        }
        public String getCodeBarre() {
            return cols[3];
        }
        public String getNomAffiche() {
            return (getNomCourt().length() > 0) ? getNomCourt() : getNom();
        }
    }

    private final ArrayList<Produit> produits = new ArrayList<>(10000);
    private final ArrayList<Produit> produitsFiltres = new ArrayList<>(10000);
    private ArrayList<Produit> vue = produits;
 
    public ExtractionODOO() {

    }
     
    @Override
    public String getColumnName(int column) {
        return Produit.COL_NAMES[column];
    }
 
    @Override
    public Class<?> getColumnClass(int column) {
        return Produit.COL_CLASSES[column];
    }
 
    @Override
    public int getColumnCount() {
        return Produit.COL_NAMES.length;
    }
 
    @Override
    public int getRowCount() {
        return vue.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return vue.get(rowIndex).getColumn(columnIndex);
    }
    public Produit getRow(int index) {
        return vue.get(index);
    }
	public void filter(String text) {
        produitsFiltres.clear();
        if (null == text || ((text = text.trim()).length() == 0)) {
            vue = produits;
        } else {
            vue = produitsFiltres;
            text = text.toLowerCase();
            for (Produit p : produits) {
                if (p.match(text)) {
                    produitsFiltres.add(p);
                }
            }
        }
        fireTableDataChanged();
	}
    void update(File f, Component errorDialogComponent) {
        produits.clear();
        produitsFiltres.clear();
        vue = produits;
        try (CsvReader reader = new CsvReader(f)) {
            String header[] = reader.readLine();
            if (null != header) {
                int indexRef = -1;
                int indexNom = -1;
                int indexNomCourt = -1;
                int indexCodeBarre = -1;
                for (int i = 0; i < header.length; ++i) {
                    switch(header[i]) {
                        case "Référence interne": indexRef = i; break;
                        case "Nom": indexNom = i; break;
                        case "Nom court": indexNomCourt = i; break;
                        case "Code Barre": indexCodeBarre = i; break;
                    }
                }
                if (indexRef < 0 || indexNom < 0 || indexCodeBarre < 0) {
                    ArrayList<String> manquant = new ArrayList<>(3);
                    if (indexRef < 0) { manquant.add("Référence interne"); }
                    if (indexNom < 0) { manquant.add("Nom"); }
                    if (indexCodeBarre < 0) { manquant.add("Code Barre"); }
                    StringBuilder msg = new StringBuilder("Erreur à la lecture de '");
                    msg.append(f.getName()).append("': Il manque");
                    if (manquant.size() > 1) {
                        msg.append("les champs ").append(manquant.get(0));
                        for (int i = 1; i < manquant.size(); ++i)  {
                            msg.append(", \"").append(manquant.get(i)).append("\"");
                        }
                    } else {
                        msg.append("le champ \"").append(manquant.get(0)).append("\"");
                    }
                    if (null != errorDialogComponent) {
                        JOptionPane.showMessageDialog(errorDialogComponent, msg.toString(), "Erreur", JOptionPane.ERROR_MESSAGE);
                    }
                    return ;
                }
                String produitCSV[] = null;
                int ligneIndex = 1;
                int premiereMauvaiseLigne = -1;
                int nMauvaiseLigne = 0;
                while (null != (produitCSV = reader.readLine())) {
                    ++ligneIndex;
                    if (produitCSV.length != header.length) {
                        ++nMauvaiseLigne;
                        if (premiereMauvaiseLigne == -1) {
                            premiereMauvaiseLigne = ligneIndex;
                        }
                    } else {
                        produits.add(new Produit(produitCSV[indexRef], produitCSV[indexNom], (indexNomCourt < 0) ? "" : produitCSV[indexNomCourt], produitCSV[indexCodeBarre]));
                    }
                }
                if (nMauvaiseLigne > 0 && null != errorDialogComponent) {
                    JOptionPane.showMessageDialog(errorDialogComponent, nMauvaiseLigne + " lignes sont malformées, la première est : " + premiereMauvaiseLigne, "Problème", JOptionPane.WARNING_MESSAGE);
                }
                fireTableDataChanged();
            }
        } catch (IOException e) {
            Log.log("ExtractionODOO.update(File f, Component errorDialogComponent)", e);
            if (null != errorDialogComponent) {
                JOptionPane.showMessageDialog(errorDialogComponent, "Erreur à la lecture de '"+f.getName()+"': " + e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
