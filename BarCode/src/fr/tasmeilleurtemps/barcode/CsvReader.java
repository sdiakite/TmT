package fr.tasmeilleurtemps.barcode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CsvReader implements AutoCloseable {
    private BufferedReader reader;

    CsvReader(BufferedReader reader) {
        this.reader = reader;
    }
    CsvReader(File file, Charset charset) throws FileNotFoundException {
        this(new BufferedReader(new InputStreamReader(new FileInputStream(file), charset)));
    }
	CsvReader(File file) throws FileNotFoundException {
        this(file, StandardCharsets.UTF_8);
    }
    CsvReader(String path, Charset charset) throws FileNotFoundException {
        this(new BufferedReader(new InputStreamReader(new FileInputStream(path), charset)));
    }
	CsvReader(String path) throws FileNotFoundException {
        this(path, StandardCharsets.UTF_8);
    }
    public void close() throws IOException {
        if (null != reader) {
            reader.close();
            reader = null;
        }
    }

    public String []readLine() throws IOException {
        return readLine(null);
    }
    public String []readLine(String rawline[]) throws IOException {
        String line;
        if (null == reader || null == (line = reader.readLine())) {
            return null;
        }
        if (null != rawline && rawline.length > 0) {
            rawline[0] = line;
        }
        String cols[] = line.split("\",\"");
        for (int i = 0; i < cols.length; ++i) {
            if (cols[i].length() > 1) {
                cols[i] = cols[i].substring('"' == cols[i].charAt(0) ? 1 : 0, '"' == cols[i].charAt(cols[i].length() - 1) ? cols[i].length() - 1 : cols[i].length());
            }
        }
        return cols;
    }

}
