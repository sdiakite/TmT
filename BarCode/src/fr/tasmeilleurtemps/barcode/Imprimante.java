package fr.tasmeilleurtemps.barcode;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.ArrayList;

public class Imprimante {
    public final String name;
    ArrayList<String> jobNames = new ArrayList<>(4096);
    ArrayList<File> jobFiles = new ArrayList<>(4096);
    public Imprimante(String name) {
        this.name = name;
    }
    public String toString() { return name; }

    static String extractLpOutput(String prefix, String output, String errPrefix) {
        if (!output.startsWith(prefix)) {
            throw new RuntimeException(errPrefix + " : \""+ output +"\"");
        }
        int spaceIndex = output.indexOf(" ", prefix.length() + 1);
        if (spaceIndex < 0) {
            throw new RuntimeException(errPrefix + " : \""+ output +"\"");
        }
        return output.substring(prefix.length(), spaceIndex).trim();
    }
    public synchronized String imprime(File path, int nombreDEtiquettes) throws IOException {
        Processus p = new Processus("/usr/bin/lp", "-d", name, path.getAbsolutePath(), "-n", String.valueOf(nombreDEtiquettes));
        if (p.returnCode == 0 && p.out.size() > 0) {
            String jobName = extractLpOutput("request id is ", p.out.get(0), "Format de sortie de cups non reconnu");
            jobNames.add(jobName);
            jobFiles.add(path);
            return jobName;
        }
        throw new RuntimeException("Impossible d'imprimer sur \"" + name + "\"");
    }
    public String imprime(BufferedImage img, int nombreDEtiquettes) throws IOException {
        File imageImprimable = File.createTempFile("code_barre_", ".png");
        ImageIO.write(img, "png", imageImprimable);
        return imprime(imageImprimable, nombreDEtiquettes);
    }

    public synchronized void cleanDoneJobs() {
        try {
            Processus p = new Processus("/usr/bin/lpstat", "-o", name);
            if (p.returnCode == 0) {
                StringBuilder lignesBuf = new StringBuilder();
                for (String ligne : p.out) {
                    lignesBuf.append(ligne).append('|');
                }
                String enCours = lignesBuf.toString();
                for (int i = 0; i < jobNames.size();) {
                    if (!enCours.contains(jobNames.get(i))) {
                        Log.log("Imprimante.cleanDoneJobs()", "Nettoyage job : ", jobNames.get(i));
                        jobNames.remove(i);
                        File f = jobFiles.remove(i);
                        f.delete();
                    } else {
                        ++i;
                    }
                }
            }
        } catch (IOException e) {
            Log.log("Imprimante.cleanDoneJobs()", e);
        }
    }
    
    public static Vector<Imprimante> list() throws IOException {
        Processus p = new Processus("/usr/bin/lpstat", "-p");
        if (p.returnCode == 0) {
            Vector<Imprimante> imprimantes = new Vector<>(p.out.size());
            for (int i = 0; i < p.out.size(); ++i) {
                if (p.out.get(i).startsWith("printer ")) {
                    String name = extractLpOutput("printer ", p.out.get(i), "Format de nom d'imprimante non reconnu");
                    imprimantes.add(new Imprimante(name));
                }
            }
            return imprimantes;
        }
        StringBuilder buf = new StringBuilder();
        for (String s : p.err) {
            buf.append(s + "\n");
        }
        throw new RuntimeException(buf.toString());
    }
}
