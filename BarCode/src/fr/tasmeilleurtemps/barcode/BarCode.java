package fr.tasmeilleurtemps.barcode;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import java.awt.image.BufferedImage;
import java.awt.Font;
import java.awt.Graphics2D;

public class BarCode {
    // Résolution de l'imprimante
    static final int POINTS_PAR_POUCES = 203;
    // Largeur des étiquettes en pouces
	static final double L = 1.25;
    // Hauteur des étiquettes en pouces
    static final double H = 1.;
    // Largeur en pixels
    static final int L_PX = (int)(POINTS_PAR_POUCES * L);
    // Hauteur en pixels
    static final int H_PX = (int)(POINTS_PAR_POUCES * H);
    public static final BufferedImage MAUVAIS_CODE_BARRE = BarCode.make("000000000000", new String[]{ "Code-barre", "! INVALIDE !"});

    
    public static BufferedImage make(String code, String label[], BufferedImage finalImg) {
        try {
            // On enlève le code de véfication
            String codeSansChecksum = (code.length() > 12) ? code.substring(0, 12) : code;
            Barcode barcode = BarcodeFactory.createEAN13(codeSansChecksum);
            barcode.setFont(Font.decode("Arial-Monospace-22"));
            BufferedImage img = BarcodeImageHandler.getImage(barcode);
            if (null == finalImg) {
                finalImg = new BufferedImage(
                    L_PX, H_PX, img.getType(), (java.awt.image.IndexColorModel)img.getColorModel()
                );
            }
			for (int x = 0; x < L_PX; ++x) {
				for (int y = 0; y < H_PX; ++y) {
					finalImg.setRGB(x, y, 0xFFFFFFFF);
				}
			}
			int off_x = (L_PX - img.getWidth()) / 2;
			int off_y = (H_PX - img.getHeight()) - 5;
			for (int x = 0; x < img.getWidth(); ++x) {
				for (int y = 0; y < img.getHeight(); ++y) {
					finalImg.setRGB(off_x + x, off_y + y, img.getRGB(x, y));
				}
			}
			Graphics2D g2d = finalImg.createGraphics();
			g2d.setPaint(java.awt.Color.BLACK);
			g2d.setFont(Font.decode("Arial-Monospace-24"));
            java.awt.FontMetrics fm = g2d.getFontMetrics();
            

            int y = (off_y - (label.length - 1) * fm.getHeight()) / 2;// (off_y - label.length * fm.getHeight()) / 2;
            if (y < 5) { y = 5; }
            for (String s : label) {
                s = s.trim();
                int x = (finalImg.getWidth() - fm.stringWidth(s))  / 2;
                if (x < 5) { x = 5; }
                g2d.drawString(s, x, y);
                y += fm.getHeight();
            }
            g2d.dispose();
        } catch (BarcodeException|OutputException e) {
            Log.log("BufferedImage BareCode.make(String code, String label[], BufferedImage finalImg)", e);
            return MAUVAIS_CODE_BARRE;
        }
        return finalImg;
    }
    public static BufferedImage make(String code, String label[]) {
        return make(code, label, null);
    }
}
