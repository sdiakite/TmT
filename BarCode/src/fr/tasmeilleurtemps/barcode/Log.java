package fr.tasmeilleurtemps.barcode;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.ArrayList;
import java.util.Date;

public class Log {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss :");
    private static final Object LOCK = new Object();
    private static PrintStream PS = null;
    private static ThreadLocal<StringBuilder> BUFFER = new ThreadLocal<StringBuilder>() {
        @Override
        protected StringBuilder initialValue() { return new StringBuilder(512); }  
    };
    private static synchronized PrintStream getPS() {
        if (PS == null) {
            try {
                File f = File.createTempFile("code_barre_log_", ".txt");
                PS = new PrintStream(f);
                System.err.println(DATE_FORMAT.format(new Date()) + " Fichier de logs : \""+f.getAbsolutePath()+"\"");
            } catch (IOException e) {
            }
        }
        return PS;
    }
    public static void log(Object ...argv) {
        StringBuilder buf = BUFFER.get();
        buf.append(DATE_FORMAT.format(new Date()));
        
        boolean first = true;
        for (Object o : argv) {
            if (first) {
                first = false;
                buf.append(" ");
            } else {
                buf.append(", ");
            }
            buf.append(o);
        }
        buf.append('\n');
        getPS().append(buf);
        System.err.append(buf);
        buf.setLength(0);
    }
}
