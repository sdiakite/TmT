package fr.tasmeilleurtemps.barcode;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Processus {
    public final int returnCode;
    public final ArrayList<String> out = new ArrayList<>(4096);
    public final ArrayList<String> err = new ArrayList<>(4096);
    public Processus(String path, File dir, String ...argv) throws IOException {
        StringBuilder log = new StringBuilder("Execution d'un processus : `");
        log.append(path);
        if (null == argv) {
            argv = new String[] {};
        }
        String cmd[] = new String[1 + argv.length];
        cmd[0] = path;
        for (int i = 0; i < argv.length; ++i) {
            cmd[i + 1] = argv[i];
            log.append(' ').append(argv[i]);
        }
        log.append('`');
        if (path != "/usr/bin/lpstat" && argv.length != 2 && argv[0] != "-o") {
            Log.log(log);
        }
        Process p = Runtime.getRuntime().exec(cmd, null, dir);
        int status = Integer.MIN_VALUE;
        while (Integer.MIN_VALUE == status) {
            try {
                status = p.waitFor();
                if (Integer.MIN_VALUE == status) {
                    status = -1;
                }
            } catch(InterruptedException e) {}
        }
        returnCode = status;
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            while (null != (line = reader.readLine())) {
                out.add(line);
            }
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
            while (null != (line = reader.readLine())) {
                err.add(line);
            }
        }
    }
    public Processus(String path, String ...argv) throws IOException {
        this(path, null, argv);
    }
}
